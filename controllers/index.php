<?php
	function login(){
		$username = $_POST["username"];
		$password = $_POST["password"];

		if(Users::logIn($username, $password)){
			Main::$folder = "home";
		} else {
			View::setVariable("error", true);
		}
	}

	function logout(){
		Users::logOut();
		Main::$folder = "login";
	}

	function changePage(){
		if($str = $_POST["param1"]){
			$arr = explode("/", $str);

			Main::$folder = $arr[0];

			if(isset($arr[1])){
				Main::$page = $arr[1];
			} else {
				Main::$page = "index";
			}
		}
	}

	function modalUpdate(){
		Modal::update($_POST);

	}
?>
