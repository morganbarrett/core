<?php
	ini_set('display_errors', 1);

	define("CLIENT_PATH", "../client/");

	function hidie($str = "HI!"){
		if(is_array($str)){
			print_r($str);
		} else {
			echo $str;
		}

		die();
	}

	class Main {
		public static $root = "C:/Program Files (x86)/Ampps/www/bts/";
		public static $client;
		public static $modules;
		public static $pages;
		public static $folder, $page;

		public static $searchPaths = array("core");

		public static function init($options){
			$setup = isset($_GET["setup"]) && $_GET["setup"] == "true";

			self::$client = $options["client"];
			self::$modules = $options["modules"];
			self::$pages = $options["pages"];

			foreach(self::$modules as $module) {
				self::addSearchPath("modules/".$module);
			}

			self::addSearchPath(self::$client);

			foreach(self::$searchPaths as $path){
				self::loadFolder($path."/lib", $setup);
			}

			$requestedURI = $_SERVER["REQUEST_URI"];
			$requestedExt = pathinfo($requestedURI, PATHINFO_EXTENSION);

			if($requestedExt == "scss"){
				echo CSS::compile($options["colours"]);
			} else {
				\php_error\reportErrors();

				$loggedIn = Users::loggedIn();

				if(isset($_GET["folder"]) && isset($_GET["page"])){
					self::$folder = Utils::def($_GET["folder"], "home");
					self::$page = Utils::def($_GET["page"], "index");
				}

				if(!$loggedIn && self::$folder != "frontend"){
					self::$folder = "login";
					self::$page = "index";
				}

				if(Utils::getIndex($_SERVER, "HTTP_X_REQUESTED_WITH") == "XMLHttpRequest"){
					self::$folder = "ajax";
					self::$page = $_POST["ajax"];
				} else if($_SERVER["REQUEST_METHOD"] == "POST"){
					self::loadContoller();
				}

				self::loadModel();
			}
		}

		public static function addSearchPath($path){
			array_push(self::$searchPaths, $path);
		}

		public static function loadFolder($folder, $setup = false){
			$files = scandir(self::$root.$folder, 1);
			$loaded = array();

			for($i = 0; $i < count($files) - 2; $i++){
				$file = $files[$i];
				$name = pathinfo($file, PATHINFO_FILENAME);
				$extension = pathinfo($file, PATHINFO_EXTENSION);

				if($extension == "php"){
					require(self::$root.$folder."/".$name.".php");
					array_push($loaded, $name);
				}
			}

			foreach($loaded as $class) {
				if($setup && method_exists($class, "setup")){
					$class::setup();
				}

				if(method_exists($class, "init")){
					$class::init();
				}
			}
		}

		private static function loadContoller(){
			foreach(self::$searchPaths as $path){
				include(Main::$root.$path."/controllers/index.php");
			}

			$folder = self::$folder;
			$page = self::$page;

			$conPath = "../".self::$client."/controllers/$folder.php";

			if(file_exists($conPath)){
				require($conPath);
			}

			Utils::runControl($_POST["control"]);

			$folder = self::$folder;
			$page = self::$page;

			header("Location: index.php?folder=$folder&page=$page");
			exit;
		}

		private static function loadModel(){
			require(Main::$root."core/models/index.php");

			$folder = self::$folder;
			$page = self::$page;

			$modelPath = "../".self::$client."/models/$folder.php";

			if(file_exists($modelPath)){
				require($modelPath);
				$funcName = $page."Model";

				if(function_exists($funcName)){
					$funcName();
				} else {
					require("models/404.php");
				}
			} else {
				require("models/404.php");
			}

			View::set("index");
		}
	}
?>
