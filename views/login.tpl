<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>BTS</title>

		<link rel="stylesheet" type="text/css" href="../core/includes/css/theme.css">
		<include>js</include>

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="am-splash-screen">
		<include>common</include>

		<div class="am-wrapper am-login">
			<div class="am-content">
				<div class="main-content">
					<div class="login-container">
						<div class="panel panel-default">
							<include>body</include>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
