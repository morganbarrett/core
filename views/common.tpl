<function set="controller">
	<a href="#">
		<var get="label"></var>
	</a>
</function>

<function set="outputTable">
	<table class="table table-hover">
		<tr>
			<foreach values="table.columns">
				<th>
					<var get="value"></var>
				</th>
			</foreach>
		</tr>

		<foreach values="table.rows">
			<if true="filterName != 'category' || filter == value.category">
				<tr>
					<foreach values="value">
						<td data-toggle="modal" data-target="#form-bp1">
							<var get="value"></var>
						</td>
					</foreach>
				</tr>
			</if>
		</foreach>
	</table>
</function>

<function set="tabs">
	<div class="tab-container">
		<ul class="nav nav-tabs">
			<foreach values="tabs">
				<li>
					<wrap>a href="#<var get="key"></var>" data-toggle="tab" aria-expanded="false"</wrap>
						<var value="label"></var>
					</a>
				</li>
			</foreach>
		</ul>
		<div class="tab-content">
			<div id="home" class="tab-pane cont">
				<p> Consectetur adipisicing elit. Ad aperiam dolore veniam mollitia consectetur aut. Cumque sunt consequatur, officiis voluptatum quas atque magnam animi eaque facere cupiditate quos ad totam saepe porro nostrum tenetur. Assumenda esse quidem, sed vel dolore quisquam fuga culpa non, ducimus, impedit fugiat vero similique recusandae?</p>
				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam dolore veniam mollitia consectetur aut. Cumque sunt consequatur, officiis voluptatum quas atque magnam animi eaque facere cupiditate quos ad totam saepe porro nostrum tenetur. Assumenda esse quidem, sed vel dolore quisquam fuga culpa non, ducimus, impedit fugiat vero similique recusandae?</p>
			</div>
			<div id="profile" class="tab-pane cont">
				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima praesentium laudantium ipsa, enim maxime placeat, dolores quos sequi nisi iste velit perspiciatis rerum eveniet voluptate laboriosam perferendis ipsum. Expedita, maiores.</p>
				<p> Consectetur adipisicing elit. Minima praesentium laudantium ipsa, enim maxime placeat, dolores quos sequi nisi iste velit perspiciatis rerum eveniet voluptate laboriosam perferendis ipsum. Expedita, maiores.</p>
			</div>
			<div id="messages" class="tab-pane active">
				<p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more</a>
			</div>
		</div>
	</div>
</function>