<function set="dropdownIcon">
	<li class="dropdown">
		<a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
			<function call="icon"><var get="icon"></var></function>
		</a>
		<ul class="dropdown-menu am-connections">
			<var get="content"></var>
		</ul>
	</li>
</function>


<function set="dropdownLayout">
	<li>
		<div class="title">
			<var get="title"></var>
		</div>
		
		<div class="list">
			<div class="content">
				<ul>
					<var get="content"></var>
				</ul>
			</div>
		</div>
		<div class="footer">
			<var get="footer"></var>
		</div>
	</li>
</function>

<if true="loggedIn">
	<nav class="navbar navbar-default navbar-fixed-top am-top-header">
		<div class="container-fluid">
			<a href="#" class="am-toggle-right-sidebar" onclick="$('.am-right-sidebar').css('right','0')">
				<function call="icon">home</function>
			</a>
			<div class="navbar-header">
				<div class="page-title">
					<span><var get="pages.$currentFolder.label"></var></span>
				</div>

				<a href="#" class="am-toggle-left-sidebar navbar-toggle collapsed" aria-expanded="false" aria-controls="sidebar-elements">
					<i class="glyphicon glyphicon-envelope"></i>
				</a>

				<a href="index.php" class="navbar-brand"></a>
			</div>

			<a href="#" data-toggle="collapse" data-target="#am-navbar-collapse" class="am-toggle-top-header-menu collapsed">
				<span class="glyphicon glyphicon-menu-down"></span>
			</a>

			<div id="am-navbar-collapse" class="collapse navbar-collapse">
				<h2 class="nav-title"><var get="pages.$currentFolder.label"></var></h2>

				<ul class="nav navbar-nav am-nav-left">
					<foreach values="pages.$currentFolder.sub">
						<li>
							<function call="changePage">
								<var set="section"><var get="currentSection"></var></var>
								<var set="page"><var get="key"></var></var>
								<var set="label"><var get="value"></var></var>
							</function>
						</li>
					</foreach>
				</ul>

				<ul class="nav navbar-nav navbar-right am-user-nav">
					<function call="dropdownIcon">
						<var set="icon">user</var>
						<var set="content">
							<li>
								<function call="changePage">
									<var set="section">users</var>
									<var set="page">profile</var>
									<var set="get">UserID=1</var>
									<var set="label"><span class="glyphicon glyphicon-home"></span> My profile</var>
								</function>
							</li>
							<li><a href="#"><span class="glyphicon glyphicon-home"></span> Settings</a></li>
							<li><a href="#"><span class="glyphicon glyphicon-home"></span> Help</a></li>
							<li>
								<function call="controller">
									<var set="control">logout</var>
									<var set="label"><span class="glyphicon glyphicon-home"></span> Sign Out</var>
								</function>
							</li>
						</var>
					</function>
				</ul>

				<ul class="nav navbar-nav navbar-right am-icons-nav">
					<function call="dropdownIcon">
						<var set="icon">comment</var>
						<var set="title">
							Messages <span class="badge">3</span>
						</var>
						<var set="content">
							<li class="active">
								<a href="#">
									<div class="logo"><img src="assets/img/avatar2.jpg"></div>
									<div class="user-content">
										<span class="date">April 25</span>
										<span class="name">Jessica Caruso</span>
										<span class="text-content">Request you to be a part of the same so that we can work...</span>
									</div>
								</a>
							</li>
						</var>
						<var set="footer">
							<a href="#">View all notifications</a>
						</var>
					</function>

					<function call="dropdownIcon">
						<var set="icon">flag</var>
						<var set="title">
							Notifications <span class="badge">3</span>
						</var>
						<var set="content">
							<li class="active">
								<a href="#">
									<div class="logo">
										<i class="glyphicon glyphicon-home"></i>
									</div>
									<div class="user-content">
										<span class="circle"></span>
										<span class="name">Jessica Caruso</span>
										<span class="text-content"> accepted your invitation to join the team.</span>
										<span class="date">2 min ago</span>
									</div>
								</a>
							</li>
						</var>
						<var set="footer">
							<a href="#">View all notifications</a>
						</var>
					</function>

					<function call="dropdownIcon">
						<var set="icon">home</var>
						<var set="title">Connections</var>
						<var set="content">
							<li>
								<div class="logo"><img src="assets/img/github.png"></div>
								<div class="field">
									<span>GitHub</span>
									<div class="pull-right">
										<div class="switch-button switch-button-sm">
											<input type="checkbox" checked="" name="check1" id="switch1">
											<span>
												<label for="switch1"></label>
											</span>
										</div>
									</div>
								</div>
							</li>
						</var>
						<var set="footer">
							<a href="#">View all connections</a>
						</var>
					</function>
				</ul>
			</div>
		</div>
	</nav>
</var>