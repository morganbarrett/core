<div id="form-bp1" tabindex="-1" role="dialog" class="modal fade modal-colored-header in">
	<div class="modal-dialog custom-width">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
				<h3 class="modal-title">Form Modal</h3>
			</div>
			<div class="modal-body form">
				<div class="form-group">
					<label>Email address</label>
					<input type="email" placeholder="username@example.com" class="form-control">
				</div>
				<div class="form-group">
					<label>Your name</label>
					<input type="name" placeholder="John Doe" class="form-control">
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label>Your birth date</label>
					</div>
				</div>
				<div class="row no-margin-y">
					<div class="form-group col-xs-3">
						<input type="name" placeholder="DD" class="form-control">
					</div>
					<div class="form-group col-xs-3">
						<input type="name" placeholder="MM" class="form-control">
					</div>
					<div class="form-group col-xs-3">
						<input type="name" placeholder="YYYY" class="form-control">
					</div>
				</div>
				<p>
					<input type="checkbox" name="c[]" checked="">  Send me notifications about new products and services.
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
				<button type="button" data-dismiss="modal" class="btn btn-primary md-close">Proceed</button>
			</div>
		</div>
	</div>
</div>