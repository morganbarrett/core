<if true="loggedIn">
	<div class="am-left-sidebar">
		<div class="content">
			<div class="am-logo"></div>

			<ul class="sidebar-elements">
				<foreach values="pages">
					<wrap>li class="parent<if true="key == currentFolder"> active</if>"</wrap>
						<function call="changePage" class="btn-lg">
							<var set="section"><var get="key"></var></var>
							<var set="page">index</var>
							<var set="label">
								<function call="icon"><var get="icon"></var></function>
								<span><var get="label"></var></span>
							</var>
						</function>

						<ul class="sub-menu">
							<li class="title">
								<var get="label"></var>
							</li>

							<li class="nav-items">
								<div class="am-scroller">
									<div class="content">
										<ul>
											<var set="section"><var get="key"></var></var>
											<foreach values="sub">
												<wrap>li<if true="currentFolder == section && currentPage == key"> class="active"</if></wrap>
													<function call="changePage">
														<var set="page"><var get="key"></var></var>
														<var set="label"><var get="value"></var></var>
													</var>
												</li>
											</foreach>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</li>
				</foreach>
			</ul>
		</div>
	</div>
</if>