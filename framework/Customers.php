<?php
	class Customers {
		public static function setup(){
			SQL::createTable(array(
				"drop" => true,
				"section" => "Framework",
				"class" => "Customers",
				"table" => "",
				"columns" => array(
					array("INT", "ID", "AUTO_INCREMENT"),
					array("TEXT", "Name"),
					array("TEXT", "Email")
				),
				"keys" => array(
					array("PRIMARY KEY", "ID")
				),
				"values" => array(
					array("Name" => "Bob", "Email" => "bob@test.com"),
					array("Name" => "John", "Email" => "john@test.com"),
					array("Name" => "Tony", "Email" => "tony@test.com")
				)
			));
		}

		public static function get($columns = array("*")){
			return SQL::select(array(
				"columns" => $columns,
				"section" => "Framework",
				"class" => "Customers",
				"table" => "",
			))->query();
		}

		public static function getID($id){
			return SQL::select(array(
				"section" => "Framework",
				"class" => "Customers",
				"table" => "",
				"where" => SQL::equals("ID", $id)
			))->query()[0];
		}

		public static function outputTable($options){
			$modal = "";

			if($options["modal"]){
				$modal = "customer";
			}

			return HTML::renderTable(array(
				"columns" => $options["columns"],
				"rows" => self::get($options["columns"]),
				"modal" => $modal
			));
		}
	}
?>