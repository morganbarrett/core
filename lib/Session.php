<?php
	session_start();

	class Session {
		public static function get($key){
			return Utils::getIndex($_SESSION, $key);
		}

		public static function set($key, $value){
			$_SESSION[$key] = $value;
		}
	}
?>
