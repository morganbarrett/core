<?php
	class SQLObj {
		private $mysqli;
		private $queryString;

		public function __construct($mysqli, $queryString){
			$this->mysqli = $mysqli;
			$this->queryString = $queryString;
		}

		public function query($debug = false){
			if($debug){
				echo $this->queryString;
			}

			$result = $this->mysqli->query($this->queryString);

			if($result){
				$rows = array();

				while($row = $result->fetch_assoc()){
					array_push($rows, $row);
				}

				if($debug){
					print_r($rows);
				}

				return $rows;
			}
			
			return false;
		}
	}

	class SQL {
		private static $mysqli;

		public static function init(){
			$database = "bts";
			$hostname = "localhost";
			$username = "root";
			$password = "root";

			self::$mysqli = new mysqli($hostname, $username, $password, $database);

			if(mysqli_connect_errno()){
				printf("Connect failed: %s", mysqli_connect_error());
				exit();
			}
		}

		public static function escape($string){
			return self::$mysqli->real_escape_string($string);
		}
		
		public static function select($options){
			$table = $options["table"];
			
			$columnsStr = "*";
			if($columns = Utils::getIndex($options, "columns")){
				$columnsStr = implode(",", $columns);
			}

			$orderStr = "";
			if($order = Utils::getIndex($options, "order")){
				$orderStr = "ORDER BY ".implode(",", $order);
			}

			$groupStr = "";
			if($group = Utils::getIndex($options, "group")){
				$groupStr = "GROUP BY ".$group;
			}
			
			$whereStr = "";
			if($where = Utils::getIndex($options, "where")){
				$whereStr = "WHERE ".$where;
			}

			$query = "SELECT $columnsStr FROM $table $whereStr $orderStr $groupStr";

			 if($alias = Utils::getIndex($options, "alias")){
				 $query = "(".$query.") AS ".$alias;
			 }

			 return new SQLObj(self::$mysqli, $query);
		}

		public static function insert($options){
			$table = $options["table"];
			$columns = array();
			$values = array();

			foreach($options["values"] as $key => $value) {
				$columns[] = $key;
				$values[] = $value;
			}

			$columns = implode(", ", $columns);
			$values = implode("', '", $values);

			$mysqli->query("INSERT INTO $table ($columns) VALUES ('$values')");
		}

		public static function dataType($field, $type, $special = ""){
			return "$field $type $special";
		}

		public static function equals($key, $value){
			return $key." = '".$value."'";
		}

		public static function andOp($arr){
			return implode(" AND ", $arr);
		}

		public static function createTable($options){
			$table = $options["section"].$options["class"].$options["table"];
			$columns = array();

			foreach($options["columns"] as $column){
				array_push($columns, $column[1]." ".$column[0]." ".$column[2]);
			}

			foreach($options["keys"] as $key){
				array_push($columns, $key[0]." (".$key[1].")");
			}

			$columns = implode(", ", $columns);

			if($options["drop"]){
				$mysqli->query("DROP TABLE $table");
			}

			$mysqli->query("CREATE TABLE $table ($columns)");

			if(is_array($options["values"])){
				foreach($options["values"] as $values){
					self::insert(array(
						"section" => $options["section"],
						"class" => $options["class"],
						"table" => $options["table"],
						"values" => $values
					));
				 }
			 }
		}

/*
		exports.insert = function(options){
			var fields = [], values = [];

			for(var i in options.values){
				fields.push(i);
				values.push(options.values[i]);
			}

			 return new SQLObj(
				 "INSERT INTO " + options.table + " " +
				 "(" + fields.join(",") + ") " +
				 "VALUES ('" + values.join("','") + "')"
			 );
		}

		exports.update = function(options){
			var values = [];

			for(var i in options.values){
				values.push(i + "='" + options.values[i] + "'");
			}

			 return new SQLObj(
				 "UPDATE " + options.table + " " + 
				 "SET " + values.join(",") + " " +
				 formatWhere(options.where)
			 );
		}

		exports.delete = function(options){
			 return new SQLObj(
				 "DELETE FROM " + options.table + " " +
				 formatWhere(options.where)
			 );
		}
		*/
	}
?>