<?php
	class View {
		private static $variables = array();
		private static $functions = array();
		private static $includes = array();

		public static function set($name, $template = "default"){
			$html = "";

			foreach(Main::$searchPaths as $path){
				if($file = Utils::readFile(Main::$root.$path."/views/$template.tpl")){
					$html = $file;
					break;
				}
			}
			
			//$js = array();

			$js = array_merge(
				Utils::listFolder("../core/includes/js"),
				Utils::listFolder("includes/js")
			);

			self::setInclude("js", $js);
			self::setInclude("common", Utils::readFile(Main::$root."core/views/common.tpl"));
			self::setInclude("leftSidebar", Utils::readFile(Main::$root."core/views/leftSidebar.tpl"));
			self::setInclude("rightSidebar", Utils::readFile(Main::$root."core/views/rightSidebar.tpl"));
			self::setInclude("messages", Utils::readFile(Main::$root."modules/messaging/views/footer.tpl"));
			self::setInclude("nav", Utils::readFile(Main::$root."core/views/nav.tpl"));
			self::setInclude("body", Utils::def(
				Utils::readFile(Main::$root.Main::$client."/views/".Main::$folder."/$name.tpl"),
				Utils::readFile(Main::$root."core/views/404.tpl")
			));

			$templeto = new Templeto($html, array(
				"variables" => self::$variables,
				"functions" => self::$functions,
				"includes"	=> self::$includes
			));

			echo $templeto->getHTML();
			die();
		}

		public static function setVariable($key, $value){
			self::$variables[$key] = $value;
		}

		public static function setFunction($key, $value){
			self::$functions[$key] = $value;
		}

		public static function setInclude($key, $value){
			self::$includes[$key] = $value;
		}
	}
?>
