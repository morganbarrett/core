<?php
	class Record {
		private $table;
		private $values;

		public function __construct($table, $values = array()){
			$this->table = $table;
			$this->values = $values;
		}

		public function getField($field){
			return Utils::getIndex($this->values, $field);
		}

		public function setField($field, $value){
			$this->values[$field] = $value;
		}

		public function save(){

		}
	}
?>