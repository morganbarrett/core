<?php
	abstract class Table {
		protected static $name;
		protected static $columns = array("ID");

		public static function create(){
			SQL::createTable(array(
				"table" => static::$name,
				"columns" => array(
					array("INT", "ID", "AUTO_INCREMENT"),
					array("INT", "TablesNamesID")
				),
				"keys" => array(
					array("PRIMARY KEY", "ID")
				)
			));
		}

		public static function createRecord($values = array()){
			return new Record(static::$name, $values);
		}

		public static function getTable(){
			return array(
				"columns" => static::$columns,
				"rows" => static::getRecords(array("values" => true))
			);
		}

		public static function getRecords($options = array()){
			$rows = SQL::select(array(
				"columns" => static::$columns,
				"table"   => static::$name
			))->query();

			if($rows){
				$records = array();

				foreach($rows as $row){
					$push = $row;

					if(!$options["values"]){
						$push = new Record(static::$name, $row);
					}

					array_push($records, $push);
				}

				return $records;
			} else {
				return false;
			}
		}

		public static function getRecord($id){
			$query = SQL::select(array(
				"columns" => $columns,
				"table"   => static::$name,
				"where"   => SQL::equals("ID", $id) 
			));

			return new Record(static::$name, $query->query());
		}
	}
?>