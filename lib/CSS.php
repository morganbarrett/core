<?php
	class CSS {
		public static function compile($colours = array()){
			$files = array();

			foreach(Main::$searchPaths as $path){
				$files = array_merge($files,
					Utils::listFolder(Main::$root.$path."/includes/css", "scss")
				);
			}	

			$css = "";

			foreach($colours as $name => $colour){
				$css .= "\$color_".$name.": ".$colour.";";
			}

			foreach($files as $file){
				$css .= Utils::readFile($file);
			}

			$scss = new scssc();
			return $scss->compile($css);
		}
	}
?>