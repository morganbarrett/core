<?php
	class Users {
		public static function setup(){
			SQL::createTable(array(
				"section" => "Classes",
				"class"   => "Session",
				"table"   => "Users",
				"columns" => array(
					array("INT", "ID", "AUTO_INCREMENT"),
					array("TEXT", "Username"),
					array("TEXT", "Password")
				),
				"keys" => array(
					array("PRIMARY KEY", "ID")
				),
				"values" => array(
					array(
						"Username" => "morgan",
						"Password" => "da8bee3a29eeb9abb543a9a918eabd56"
					)
				)
			));
		}

		public static function logIn($username, $password){
			if(isset($username)){
				$passwordHash = md5($password."bob");

				$rows = SQL::select(array(
					"columns" => array("ID"),
					"table"   => "Users",
					"where"   => SQL::andOp(array(
						SQL::equals("Username", $username),
						SQL::equals("Password", $passwordHash)
					))
				))->query();

				if($rows && $row = $rows[0]){
					$_SESSION["ID"] = $row["ID"];
					$_SESSION["Check"] = md5($username."bob".$passwordHash);
					return true;
				}
			}

			return false;
		}

		public static function logOut(){
			session_destroy();
		}

		public static function loggedIn(){
			if(isset($_SESSION["ID"]) && isset($_SESSION["Check"])){
				$rows = SQL::select(array(
					"columns" => array("Username", "Password"),
					"section" => "Classes",
					"class"   => "Session",
					"table"   => "Users",
					"where"   => SQL::equals("ID", $_SESSION["ID"])
				))->query();

				if($rows && $row = $rows[0]){
					$check = md5($row["Username"]."bob".$row["Password"]);
					return $_SESSION["Check"] == $check;
				}
			}

			return false;
		}
	}
?>
