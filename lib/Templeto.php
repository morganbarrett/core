<?php
	class Templeto {
		public static $tags = array();
		private static $specialOptions = array("variables", "includes", "functions");

		public $dom;
		public $variables;
		public $includes;
		public $functions;

		public static function init(){
			$elements = Utils::listFolder("../core/lib/TempletoElements");

			foreach($elements as $element){
				require($element);
				$tag = pathinfo($element)["filename"];
				self::$tags[$tag] = $tag."Element";
			}
		}

		public function __construct($html, $options = array()){
			$this->dom = new DOM($html, $options);

			foreach(self::$specialOptions as $arr){
				if($value = Utils::getIndex($options, $arr)){
					$this->$arr = $value;
				}
			}

			$this->render($this->dom->root);
		}

		public function render($element){
			if($element == null){
				return;
			}

			foreach($element->children() as $child){
				if($class = Utils::getIndex(self::$tags, $child->tag)){
					$attributes = $child->getAllAttributes();
					$handled = false;

					foreach($attributes as $attr => $value){
						$method = $attr."Action";

						if(!$handled && method_exists($class, $method)){
							$class::$method($this, $child, $value);
							$handled = true;
						}
					}

					if(!$handled && method_exists($class, "defaultAction")){
						$class::defaultAction($this, $child);
					}

					$child->tag = "div";
				} else {
					$this->render($child);
				}
			}
		}

		public function getHTML(){
			return $this->dom->outertext;
		}

		public function getVariable($variable){
			if($value = $this->parseStr($variable)){
				return $value;
			}

			return "";
		}

		public function getFunction($funcName){
			if($function = Utils::getIndex($this->functions, $funcName)){
				if(is_callable($function)){
					$content = $this->getHTML();
					return $function($content, $this);
				} else {
					return $this->cloneEnv($function)->getHTML();
				}
			}

			return null;
		}

		public function parseStr($str){
			$str = str_replace(" ", "", $str);

			$delims = array(
				"&&" => function($a, $b){return $a && $b;},
				"||" => function($a, $b){return $a || $b;},
				"==" => function($a, $b){return $a == $b;},
				"!=" => function($a, $b){return $a != $b;},
				">=" => function($a, $b){return $a >= $b;},
				"<=" => function($a, $b){return $a <= $b;},
				">"  => function($a, $b){return $a >  $b;},
				"<"  => function($a, $b){return $a <  $b;}
			);

			foreach($delims as $delim => $func){
				$array = explode($delim, $str);

				if(count($array) > 1){
					$res = eval("return '".$this->parseStr($array[0])."' ".$delim." '".$this->parseStr($array[1])."';");

					for($i = 2; $i < count($array); $i++){
						$res = eval("return '".$res."' ".$delim." '".$this->parseStr($array[$i])."';");
					}

					return $res;
				}
			}

			$array = explode(".", $str);
			$refArray = $this->variables;

			foreach($array as $index){
				if($index[0] == "$"){
					$index = $this->getVariable(substr($index, 1));
				}
				if($index[0] == "'" || $index[0] == "\""){
					return substr($index, 1, strlen($index) - 2);
				}

				if($arr = Utils::getIndex($refArray, $index)){
					$refArray = $arr;
				} else {
					return null;
				}
			}

			return $refArray;
		}

		public function replace($element, $newElement, $import = false){
			$templeto = $this->cloneEnv($newElement);
			$element->outertext = $templeto->getHTML();

			if($import){
				$this->import($templeto);
			}
		}

		public function cloneEnv($html, $options = array()){
			$newOptions = array();

			foreach(self::$specialOptions as $option){
				$array = $this->$option;

				if($newArray = Utils::getIndex($options, $option)){
					$array = array_merge($array, $newArray);
				}

				$newOptions[$option] = $array;
			}

			return new Templeto($html, $newOptions);
		}

		public function import($dom){
			foreach(self::$specialOptions as $option){
				$this->$option = array_merge($this->$option, $dom->$option);
			}
		}
	}
?>
