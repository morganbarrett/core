<?php
    class CMS {
        public $sections;

        public function __construct($name){
            $file = Utils::readFile(Main::$root.Main::$client."/cms/".$name.".json");
            $json = json_decode($file);
            $this->sections = $json->sections;
        }
    }
?>
