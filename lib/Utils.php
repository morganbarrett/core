<?php
	class Utils {
		public static function def($var, $def, $overide = false){
			return $var && !$overide ? $var : $def;
		}

		public static function getIndex($array, $index){
			if(is_object($array)){
				return $array->$index;
			} else if(isset($array[$index])){
				return $array[$index];
			} else {
				return false;
			}
		}

		public static function runControl($control){
			if( $control &&
				function_exists($control)
			){
				$control();
			}
		}

		public static function findFile($src, $reverse = false){
			$paths = Main::$searchPaths;

			if($reverse){
				$paths = array_reverse($paths);
			}

			foreach($paths as $path){
				if($file = self::readFile(Main::$root.$path."/".$src)){
					return $file;
				}
			}

			return null;
		}

		public static function readFile($dir){
			if(file_exists($dir)){
				$string = "";
				$file = fopen($dir, "r");

				while($line = fgets($file)){
					$string .= $line;
				}

				fclose($file);
				return $string;
			} else {
				return false;
			}
		}

		public static function listFolder($sourceDir, $ext = "*"){
			$files = scandir($sourceDir, 1);
			$arr = array();

			for($i = 0; $i < count($files) - 2; $i++){
				$file = $files[$i];

				if($ext == "*" || $ext == pathinfo($file, PATHINFO_EXTENSION)){
					array_push($arr, $sourceDir."/".$files[$i]);
				}
			}

			return $arr;
		}

		public static function readFolder($sourceDir){
			$files = scandir($sourceDir, 1);
			$arr = array();

			for($i = 0; $i < count($files) - 2; $i++){
				array_push($arr, self::readFile($sourceDir."/".$files[$i]));
			}

			return $arr;
		}

		public static function getClasses($base){
			$arr = array();

			foreach(get_declared_classes() as $class){
				if(is_subclass_of($class, $base)){
					array_push($arr, $class);
				}
			}

			return $arr;
		}
	}
?>
