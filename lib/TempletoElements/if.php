<?php
	class IfElement {
		public static function trueAction($dom, $element, $attrValue){
			self::compare($dom, $element, $attrValue, "true");
		}

		public static function falseAction($dom, $element, $attrValue){
			self::compare($dom, $element, $attrValue, "false");
		}

		private static function compare($dom, $element, $varName, $compValue){
			$elementContent = $element->innertext;
			$element->outertext = "";

			if($varValue = $dom->getVariable($varName)){
				if($varValue == $compValue){
					$dom->replace($element, $elementContent);
				}
			}
		}
	}
?>