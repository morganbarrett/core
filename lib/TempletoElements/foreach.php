<?php
	// needs tidying
	class ForeachElement {
		public static function valuesAction($dom, $elem, $varName){
			$content = $elem->innertext;
			$elem->outertext = "";

			if($array = $dom->getVariable($varName)){
				if(is_object($array)){
					$array = get_object_vars($array);
				}

				if(is_array($array)){
					foreach($array as $key => $value){
						$variables = array(
							"key" => $key,
							"value" => $value
						);

						if(is_array($value)){
							$variables = array_merge($value, $variables);
						}

						$elem->outertext .= $dom->cloneEnv($content, array(
							"variables" => $variables
						))->getHTML();
					}
				} else {
					foreach((new DOM($array))->childNodes() as $element){
						$newdom = $dom->cloneEnv($element->innertext);
						$newdom = $newdom->cloneEnv($content);
						$elem->outertext .= $newdom->getHTML();
					}
				}
			}
		}
	}
?>
