<?php
	class VarElement {	
		public static function setAction($dom, $element, $varName){
			$element->outertext = "";
			$value = null;

			if($element->hasAttribute("copy")){
				$copyVar = $element->getAttribute("copy");
				$value = $dom->getVariable($copyVar);
			} else if($text = $element->innertext){
				$value = $dom->cloneEnv($text)->getHTML();
			}

			$dom->variables[$varName] = $value;
		}

		public static function getAction($dom, $element, $varName){
			$varValue = Utils::def($dom->getVariable($varName), "");
			if(is_array($varValue)){
				echo "What";
				print_r($varValue);
				return;
			}
			$element->outertext = $varValue;
		}
	}
?>
