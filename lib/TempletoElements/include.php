<?php
	// needs tidying
	class IncludeElement {
		public static function srcAction($dom, $element, $src){
			$dom->replace($element, Utils::findFile("views/".$src, true), true);
		}

		public static function defaultAction($dom, $element){
			$name = $element->plaintext;

			if($content = Utils::getIndex($dom->includes, $name)){
				if($name == "js"){
					$element->outertext = "";
					foreach($content as $link){

						$elem = "script";
						$opt = array(
							"src" => $link
						);

						$newElement = $dom->dom->createElement($elem);

						foreach($opt as $attr => $value) {
							$newElement->setAttribute($attr, $value);
						}

						$element->outertext .= $newElement;
					}
				} else {
					$templeto = $dom->cloneEnv($content);
					$dom->import($templeto);
					$element->outertext = $templeto->getHTML();
				}
			}
		}
	}
?>
