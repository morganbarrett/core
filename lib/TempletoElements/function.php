<?php
	class FunctionElement {
		public static function setAction($dom, $element, $funcName){
			$dom->functions[$funcName] = $element->innertext;
			$element->outertext = "";
		}

		public static function callAction($dom, $element, $funcName){
			$content = $element->innertext;

			$templeto = $dom->cloneEnv($content);
			$templeto->variables["content"] = $templeto->getHTML();

			$function = Utils::def($templeto->getFunction($funcName), "");
			$element->outertext = $function;
		}
	}
?>