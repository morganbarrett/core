function modal(name, id){
	$.post(window.location, {
		ajax: name + "Modal",
		id: id
	}, function(data){
		var fields = JSON.parse(data);

		for(var field in fields){
			$("#" + name + "Modal" + field).html(fields[field]);
			$("#" + name + "Modal" + field).val(fields[field]);
		}

		$("#" + name + "Modal").modal();
	});
}