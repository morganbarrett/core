function controller(param){
	var arr = param.split(" "),
		method = arr[0],
		inputs = "";

	for(var i = 1; i < arr.length; i++){
		inputs += "<input type='hidden' name='param" + i + "' value='" + arr[i] + "'>";
	}

	$("#controller").html(
		"<form action='' method='POST' id='controllerForm'>" +
			"<input type='hidden' name='control' value='" + method + "'>" +
			inputs +
		"</form>"
	);

	$("#controllerForm").submit();
}