<?php
	View::setVariable("loggedIn", Users::loggedIn());
	View::setVariable("pages", Main::$pages);
	View::setVariable("currentPage", Main::$page);
	View::setVariable("currentFolder", Main::$folder);

	View::setFunction("icon", function($content, $dom){
		return "<i class='glyphicon glyphicon-$content' aria-hidden='true'></i>";
	});

	View::setFunction("changePage", function($content, $dom){
		return
			"<a href='index.php".
				"?folder=".$dom->getVariable("section").
				"&page=".$dom->getVariable("page").
				//"&".$dom->getVariable("get").
			"'>".
				$dom->getVariable("label").
			"</a>";
	});
?>
